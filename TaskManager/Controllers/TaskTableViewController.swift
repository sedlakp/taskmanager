//
//  TaskTableViewController.swift
//  TaskManager
//
//  Created by Petr Sedlák on 28/07/2018.
//  Copyright © 2018 Petr Sedlak. All rights reserved.
//

import UIKit
import UserNotifications

class TaskTableViewController: UITableViewController {
    
    var task:Task?
    var selectedCategory:Category?
    var categories:[Category]=[]
    
    @IBOutlet var taskNameTextField: UITextField!
    @IBOutlet var completedButton: UIButton!
    @IBOutlet var deadlineLabel: UILabel!
    
    @IBOutlet var deadlinePicker: UIDatePicker!
    
    @IBOutlet var notificationSwitch: UISwitch!
    
    @IBOutlet var moreInfoTextView: UITextView!
    
    @IBOutlet var saveButton: UIBarButtonItem!
    
    @IBOutlet var categoryNameButton: UIButton!
    
    
    @IBAction func taskNameTextEdited(_ sender: UITextField) {
        updateSaveButtonState()
    }
    
    @IBAction func returned(_ sender: UITextField) {
        taskNameTextField.resignFirstResponder()
    }
    
    
    
    @IBAction func completeButtonTapped(_ sender: UIButton) {
        completedButton.isSelected = !completedButton.isSelected
        if completedButton.isSelected{
        notificationSwitch.isOn=false
        notificationSwitch.isEnabled=false
        }else{
        notificationSwitch.isEnabled=true
        }
    }
    @IBAction func deadlinePickerChanged(_ sender: UIDatePicker) {
        updateDeadlineLabel(date: deadlinePicker.date)
    }
    
    //actions and figure out categories
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.separatorStyle = .none
        view.addGestureRecognizer(UITapGestureRecognizer(target: view, action: #selector(UIView.endEditing(_:))))
        moreInfoTextView.layer.borderWidth=1
        moreInfoTextView.layer.borderColor = UIColor.black.cgColor
        moreInfoTextView.layer.cornerRadius=5
        categoryNameButton.setTitleColor( UIColor.white, for: .normal)
        categoryNameButton.layer.cornerRadius=5
        getCategories()
        selectedCategory=categories[2]
        fillTask()
        updateDeadlineLabel(date: deadlinePicker.date)
        updateSaveButtonState()
    }
    
    func getCategories(){
        let categories = DataService.get(type: Category.self)
        self.categories=categories
    }
    
    func fillTask(){
        if let task = task{
            taskNameTextField.text=task.taskName
            completedButton.isSelected=task.completed
            if completedButton.isSelected{
                notificationSwitch.isOn=false
                notificationSwitch.isEnabled=false
            }else{
                notificationSwitch.isOn = task.notification
            }
            completedButton.tintColor=task.category?.color?.color
            deadlinePicker.minimumDate=Date()
            deadlinePicker.date = task.date! as Date
            
            moreInfoTextView.text=task.moreInfo
             categoryNameButton.setTitle(task.category?.name, for: .normal)
            categoryNameButton.backgroundColor=task.category?.color?.color
            selectedCategory=task.category
            
        }else{
            deadlinePicker.date = Date().addingTimeInterval(86400)
            categoryNameButton.setTitle(selectedCategory?.name, for: .normal)
            categoryNameButton.backgroundColor=selectedCategory?.color?.color
            completedButton.tintColor=selectedCategory?.color?.color
            deadlinePicker.minimumDate=Date()
        }
    }

    func updateDeadlineLabel(date:Date){
        deadlineLabel.text = Task.dueDateFormatter.string(from:date)
    }
    
    func updateSaveButtonState(){
        let text = taskNameTextField.text ?? ""
        saveButton.isEnabled = !text.isEmpty
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

   //creates task and sends it to main controller
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        //super.prepare(for: segue, sender: segue)
        guard segue.identifier == "saveUnwind" else {return}
        if task == nil {
        task = Task(context: DataService.context)
        task?.uuid=UUID().uuidString
        }
        print("*******")
        task?.taskName = taskNameTextField.text
        task?.completed = completedButton.isSelected
        task?.date = deadlinePicker.date as NSDate
        task?.category = selectedCategory                      
        task?.moreInfo = moreInfoTextView.text
        task?.notification = notificationSwitch.isOn
        manageNotification()
        //print(task)
        print("preparing for segue")
        
    }
    
     let center = UNUserNotificationCenter.current()
    
    //scheduling and descheduling notifications
    func manageNotification(){
        print("hello")
        if let task=task{
            print("the notif switch is on: \(notificationSwitch.isOn)")
            
            center.getPendingNotificationRequests { (requests) in
                    DispatchQueue.main.async {
                        for request in requests{
                            print(request.identifier)
                            if request.identifier == task.uuid{
                                self.center.removePendingNotificationRequests(withIdentifiers: [task.uuid!])
                                print("old notif deleted")
                            }
                        }
                        
                        if self.notificationSwitch.isOn{
                            let content = UNMutableNotificationContent()
                            content.title = task.taskName ?? "No title"
                            content.subtitle = task.category?.name ?? " "
                            content.body = task.moreInfo ?? " "
                            content.sound=UNNotificationSound.default()
                            let date = task.date! as Date
                            let components = Calendar.current.dateComponents([.day,.hour,.minute,.month], from: date)
                            print("components: \(components)")
                            let trigger = UNCalendarNotificationTrigger(dateMatching: components, repeats: false)
                            let request = UNNotificationRequest(identifier: task.uuid!, content: content, trigger: trigger)
                        
                            self.center.add(request) { (error) in
                                if error != nil{
                                    print("an error occured")
                                }
                            }
                            print("notif created")
                        }
                }
            }
        }
    }
    
    //changes category based on what was selected in choosecategorycontroller
    @IBAction func unwindToTask(segue:UIStoryboardSegue){
        guard segue.identifier=="CategoryChange" else {return}
        
        let sourceViewController = segue.source as! ChooseCategoryTableViewController
        let chosenCategory = sourceViewController.chosenCategory!
        categoryNameButton.setTitle(chosenCategory.name, for: .normal)
        categoryNameButton.backgroundColor=chosenCategory.color?.color
        completedButton.tintColor=chosenCategory.color?.color
        self.selectedCategory=chosenCategory
        
        
    }
    

}




