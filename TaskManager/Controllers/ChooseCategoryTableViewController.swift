//
//  ChooseCategoryTableViewController.swift
//  TaskManager
//
//  Created by Petr Sedlák on 30/07/2018.
//  Copyright © 2018 Petr Sedlak. All rights reserved.
//

import UIKit

class ChooseCategoryTableViewController: UITableViewController {

    var categories:[Category]=[]
    
    var chosenCategory:Category?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.separatorStyle = .none
        getCategories()
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
        
    }

    func getCategories(){
        let categories = DataService.get(type: Category.self)
        self.categories=categories
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        if categories.count != 0{
            return categories.count
        }else{
            return 0
        }
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CategoryCell", for: indexPath) as! CategoryTableViewCell
        let category = categories[indexPath.row]
        cell.categoryNameLabel.text=category.name
        cell.categoryColorImage.tintColor=category.color?.color

        return cell
    }
 

    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard segue.identifier == "CategoryChange" else {return}
        let indexPath = tableView.indexPathForSelectedRow!
        let chosenCategory = categories[indexPath.row]
        self.chosenCategory=chosenCategory
        
    }
    
    

}








