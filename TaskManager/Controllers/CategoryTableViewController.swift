//
//  CategoryTableViewController.swift
//  TaskManager
//
//  Created by Petr Sedlák on 29/07/2018.
//  Copyright © 2018 Petr Sedlak. All rights reserved.
//

import UIKit

class CategoryTableViewController: UITableViewController {
    
    var category:Category?

    @IBOutlet var nameTextField: UITextField!
    
    @IBOutlet var colorImage: UIImageView!
    
    @IBOutlet var blueImage: UIImageView!
    @IBOutlet var brownImage: UIImageView!
    @IBOutlet var cyanImage: UIImageView!
    @IBOutlet var greenImage: UIImageView!
    @IBOutlet var magentaImage: UIImageView!
    @IBOutlet var orangeImage: UIImageView!
    @IBOutlet var purpleImage: UIImageView!
    @IBOutlet var redImage: UIImageView!
    @IBOutlet var yellowImage: UIImageView!
    
    @IBOutlet var saveButton: UIBarButtonItem!
    
    @IBOutlet var categoryNameTextField: UITextField!
    
    @IBAction func editingEnded(_ sender: Any) {
        updateSaveButtonState()
    }
    
    @IBAction func returned(_ sender: UITextField) {
        categoryNameTextField.resignFirstResponder()
    }
    
    func updateSaveButtonState(){
        let text = categoryNameTextField.text ?? ""
        saveButton.isEnabled = !text.isEmpty
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        fill()
        //had to programatically set this, if i change it only in storyboard, all cell images are black
        colorImage.tintColor=UIColor.black
        blueImage.tintColor=UIColor.blue
        brownImage.tintColor=UIColor.brown
        cyanImage.tintColor=UIColor.cyan
        greenImage.tintColor=UIColor.green
        magentaImage.tintColor=UIColor.magenta
        orangeImage.tintColor=UIColor.orange
        purpleImage.tintColor=UIColor.purple
        redImage.tintColor=UIColor.red
        yellowImage.tintColor=UIColor.yellow
        updateSaveButtonState()
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }
    
    func fill(){
        if let category=category{
            nameTextField.text=category.name
            selectedColor=category.color
            tableView.reloadData()
        }else{
            selectedColor="black"
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source
    var selectedColor:String?
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        switch indexPath{
        case [1,0]:selectedColor="black"
        case [1,1]:selectedColor="blue"
        case [1,2]:selectedColor="brown"
        case [1,3]:selectedColor="cyan"
        case [1,4]:selectedColor="green"
        case [1,5]:selectedColor="magenta"
        case [1,6]:selectedColor="orange"
        case [1,7]:selectedColor="purple"
        case [1,8]:selectedColor="red"
        case [1,9]:selectedColor="yellow"
        default:
            return

        }
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard segue.identifier=="SaveIdentifier" else{return}
        if category == nil{
        category=Category(context: DataService.context)
        }
        category?.name = nameTextField.text
        category?.color=selectedColor
    }

}













