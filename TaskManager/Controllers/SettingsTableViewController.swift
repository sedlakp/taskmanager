//
//  SettingsTableViewController.swift
//  TaskManager
//
//  Created by Petr Sedlák on 28/07/2018.
//  Copyright © 2018 Petr Sedlak. All rights reserved.
//

import UIKit
import UserNotifications

class SettingsTableViewController: UITableViewController {
    
    var sortingOption:[SortOption]=[]
    var categories:[Category]=[]
    var areNotifsOn:Bool? //have to create this as an object so i can save this.
    var tasks:[Task]=[]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.separatorStyle = .none
        getCategories()
        getTasks()
        getSortOption()
        
    }
    
   
    func getTasks(){
        let tasks = DataService.get(type: Task.self)
        self.tasks=tasks
    }
    
    func getCategories(){
        let categories = DataService.get(type: Category.self)
        self.categories=categories
    }
    
    func getSortOption(){
        let sortOption = DataService.get(type: SortOption.self)
        self.sortingOption=sortOption
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if categories.count != 0{
            return 3+categories.count
        }else{
        return 3
        }
    }
    // MARK: - Table view data source

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row{
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: "NotifCell", for: indexPath) as! ButtonTableViewCell
            cell.button.layer.cornerRadius=5
            cell.delegate=self
            return cell
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: "SortCell", for: indexPath) as! SegmentedControlTableViewCell
            cell.sortSegmentedControl.selectedSegmentIndex = Int(sortingOption[0].value)
            cell.delegate=self
            return cell
        case 2:
            let cell = tableView.dequeueReusableCell(withIdentifier: "AddCell", for: indexPath) as! AddTableViewCell
            
            return cell
        default:
            let cell = tableView.dequeueReusableCell(withIdentifier: "CategoryCell", for: indexPath) as! CategoryTableViewCell
            let category = categories[indexPath.row-3]
            cell.categoryNameLabel.text=category.name
            tableView.separatorStyle = .none
            cell.categoryColorImage.tintColor=category.color?.color
            
            return cell
            
        }
    }
    

    //allows editing of a category
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard segue.identifier == "ShowDetails" else{return}
        
        let taskController = segue.destination as! CategoryTableViewController
        let indexPath = tableView.indexPathForSelectedRow!
        let tappedCategory = categories[indexPath.row-3]
        taskController.category=tappedCategory
    }
    
    //new or edited category from categorytableviewcontroller
    @IBAction func unwindToSettings(segue: UIStoryboardSegue){
        guard segue.identifier=="SaveIdentifier" else {return}
        let sourceViewController = segue.source as! CategoryTableViewController
        if let category = sourceViewController.category{
            if let selectedIndexPath = tableView.indexPathForSelectedRow{
                categories[selectedIndexPath.row-3]=category
                print("edited at \(selectedIndexPath.row-3)")
                tableView.reloadRows(at: [selectedIndexPath], with: .none)
            }else{
                let newIndexPath = IndexPath(row: categories.count+3, section: 0)
                categories.append(category)
                print("new at \(newIndexPath.row)")
                tableView.insertRows(at: [newIndexPath], with: .automatic)
            }
        }
        DataService.saveContext()
    }

}

//handles selection of sorting optio
extension SettingsTableViewController: SegmentedCellDelegate{
    func sortingValueChanged(sender: SegmentedControlTableViewCell) {
        sortingOption[0].value=Int16(sender.sortSegmentedControl.selectedSegmentIndex)
        DataService.saveContext()
    }
}

//deschedules all notifications and sets all notification switches to off
extension SettingsTableViewController:GlobalNotificationsDelegate{
    func buttonTapped(sender: ButtonTableViewCell) {
        print("notifications will be reseted")
        let center = UNUserNotificationCenter.current()
        center.getPendingNotificationRequests { (requests) in///////// chcecks how many scheduled notifs are there before deleting them
            var i = 0
            for _ in requests{
                i+=1
                print("request no. \(i)")
            }
        }
        center.removeAllPendingNotificationRequests()
        for task in tasks{
            task.notification=false
        }
        print("notifs reseted")
        DataService.saveContext()
        
    }
    
   
    
}






