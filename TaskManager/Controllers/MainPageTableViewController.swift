//
//  MainPageTableViewController.swift
//  TaskManager
//
//  Created by Petr Sedlák on 28/07/2018.
//  Copyright © 2018 Petr Sedlak. All rights reserved.
//

import UIKit
import UserNotifications

class MainPageTableViewController: UITableViewController {
    
    var tasks:[Task]=[]
    var categories:[Category]=[]
    var sortOption:[SortOption]=[]
    

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.separatorStyle = .none
        
        getCategories()
        if categories.count==0{
            defaultCategories()
        }
        getTasks()
        if tasks.count==0{
            defaultTask()
        }
        getSortOption()
        if sortOption.count==0{
            let option = SortOption(context: DataService.context)
            option.value=0
            sortOption.append(option)
            DataService.saveContext()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        sortBy(option: sortOption[0].value)
        sortByCompletion()
        positionOfLabel=numberOfUncompleted()
        tableView.reloadData()
    }
   
    var positionOfLabel:Int=0 //tracks the position of cell with label "completed"
    
    func numberOfUncompleted()->Int{ //this returns the index.row of completed label
        var num = 0
        for task in tasks{
            guard !task.completed else {continue}
            num+=1
        }
        return num
    }
    
    func sortByCompletion(){ //completed task are on top, uncompleted are pushed to the bottom
        tasks.sort { (first, second) -> Bool in
            return !first.completed && second.completed
        }
    }
    
    
    func getTasks(){
        let tasks = DataService.get(type: Task.self)
        self.tasks=tasks
    }
    
    
    //creates default tasks
    func defaultTask(){ //can use loop to make more same entries at once
        let taskNames=["Buy milk","Go to the gym","buy a chair","mow the lawn","fill a report"]
        let categoryIndex=[2,3,0,0,1]
        let completed=[false,false,true,false,true]
        
        for num in 0...taskNames.count-1{
        let task = Task(context: DataService.context)
        task.taskName = taskNames[num]
        task.completed = completed[num]
        task.category = categories[categoryIndex[num]]
        task.moreInfo = ""
        task.uuid=UUID().uuidString
        task.notification=false
        task.date=NSDate(timeIntervalSinceNow: (TimeInterval(3600*(num+1))))
        tasks.append(task)
        }
        DataService.saveContext()
    }
    
    //creates default categories
    func defaultCategories(){
        let colors = ["black", "blue", "red", "green"]
        let names = ["Home","Work","Groceries","Activities"]
        
        for num in 0...names.count-1{
            let category = Category(context: DataService.context)
            category.name=names[num]
            category.color=colors[num]
            categories.append(category)
        }
        DataService.saveContext()
    }
    
    func getCategories(){
        let categories = DataService.get(type: Category.self)
        self.categories=categories
    }
    
    
    func getSortOption(){
        let sortOption = DataService.get(type: SortOption.self)
        self.sortOption=sortOption
    }
    
    //sorting based on selected option (A-Z is default)
    func sortBy(option:Int16){
        switch option{
        case 0:
            //by name A-Z
             tasks.sort { (first, second) -> Bool in
                first.taskName!.lowercased()<second.taskName!.lowercased()
            }
        case 1:
            //by name Z-A
            tasks.sort { (first, second) -> Bool in
                first.taskName!.lowercased()>second.taskName!.lowercased()
            }
        case 2:
            tasks.sort { (first, second) -> Bool in
                if let x = first.date as Date?, let y = second.date as Date?{
                   return x>y
                }else{
                    return true
                }
            }
        case 3:
            tasks.sort { (first, second) -> Bool in
                if let x = first.date as Date?, let y = second.date as Date?{
                    return x<y
                }else{
                    return true
                }
            }
           
        default:return
        
        }
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return tasks.count+1
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
        switch indexPath.row{
        case positionOfLabel:
            let cell = tableView.dequeueReusableCell(withIdentifier: "TitleCell", for: indexPath)
            cell.textLabel?.text="Completed"
            return cell
        default:
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "TaskCell", for: indexPath) as! TaskTableViewCell
            let task:Task
            
            //row on which task appears depends on whether it is above or below the "completed" label, either is just like the array of task or one more
            if indexPath.row<positionOfLabel{
                task = tasks[indexPath.row]
            }else{
                task = tasks[indexPath.row-1]
            }
            cell.taskNameLabel.text=task.taskName
            cell.categoryLabel.text=task.category?.name
            cell.dateLabel.text=Task.dueDateFormatter.string(from:task.date! as Date)//String(describing: task.date)
            cell.completionButton.isSelected=task.completed
            cell.completionButton.tintColor=task.category?.color?.color
            cell.delegate=self
            
            return cell
            
        }
    }
    

    
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        if indexPath.row == positionOfLabel{
            return false
        }
        return true
    }
    

    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        let deletedTask:Task
        if editingStyle == .delete {
            if indexPath.row>positionOfLabel{
                deletedTask = tasks.remove(at: indexPath.row-1)
            }else{
                deletedTask = tasks.remove(at: indexPath.row)
                positionOfLabel-=1
            }
           // let deletedTask = tasks.remove(at: indexPath.row)
            //cancel scheduled notification
            let center = UNUserNotificationCenter.current()
            center.removePendingNotificationRequests(withIdentifiers: [deletedTask.uuid!])
            //following line modifies context
            DataService.context.delete(deletedTask)
            //following line saves context, thus deleting deleted task from storage
            DataService.saveContext()
            tableView.deleteRows(at: [indexPath], with: .fade)
            
        }    
    }
 
    //sends selected task to detail view controller
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard segue.identifier == "showDetails" else{return}
        
        let taskController = segue.destination as!TaskTableViewController
        let indexPath = tableView.indexPathForSelectedRow!
        let tappedTask:Task
        if indexPath.row>positionOfLabel{
            tappedTask = tasks[indexPath.row-1]
        }else{
            tappedTask = tasks[indexPath.row]
        }
        taskController.task=tappedTask
    }
    
    //this function is connected to both back and save button, but only save button's segue has identifier "saveUnwind"
    //which saves new task or edited content
    @IBAction func unwindToMainScreen(segue: UIStoryboardSegue){
        guard segue.identifier == "saveUnwind" else {return}
        let sourceViewController = segue.source as! TaskTableViewController
        
        if let task = sourceViewController.task{
            if let selectedIndexPath = tableView.indexPathForSelectedRow{ //editing
                
                if selectedIndexPath.row>positionOfLabel{
                    tasks[selectedIndexPath.row-1]=task
                }else{
                    tasks[selectedIndexPath.row]=task
                }
                    print("edited at \(selectedIndexPath.row)")
                    tableView.reloadRows(at: [selectedIndexPath], with: .none)
                }else{ //creating new
                    let newIndexPath = IndexPath(row: tasks.count, section: 0)
                    tasks.append(task)
                print("new at \(newIndexPath.row)")
                tableView.insertRows(at: [newIndexPath], with: .automatic)
            }
        }
        DataService.saveContext()
    }

}


extension MainPageTableViewController: TaskCellDelegate{
    
    //handles what happens when you change task state as completed/not completed
    func taskCompleted(sender: TaskTableViewCell) {
        if let indexPath = tableView.indexPath(for: sender){
            var task:Task
            if indexPath.row>positionOfLabel{
                task = tasks[indexPath.row-1]
            }else{
                task = tasks[indexPath.row]
            }
            print("was task completed: \(task.completed)")
            task.completed = !task.completed
            if task.notification{ //if the notification is on manage the notification, otherwise no action is necessary
                manageNotifs(task: task)
            }
            //let indPath = IndexPath(row: indexPath.row, section: indexPath.section)
            if indexPath.row>positionOfLabel{
                //unchcecking returns task at index of one more
                tasks[indexPath.row-1]=task
                tableView.reloadRows(at: [indexPath], with: .automatic)
            }else{
                tasks[indexPath.row]=task
                tableView.reloadRows(at: [indexPath], with: .automatic)
            }
            sortBy(option: sortOption[0].value)
            sortByCompletion() //sorts new array
            let newIndexPath = IndexPath(row: getNewIndex(of: task), section: indexPath.section)

            tableView.moveRow(at: indexPath, to: newIndexPath)
            DataService.saveContext()
            
        }
    }
    
    func getNewIndex(of thisTask:Task)->Int{
        var index = -1
        for task in tasks{
            index+=1
            if task.uuid == thisTask.uuid{
                break
            }
        }

        if thisTask.completed{
            positionOfLabel-=1
            print("label position \(positionOfLabel) -")
            return index+1
        }else{
            positionOfLabel+=1
            print("label position \(positionOfLabel) +")
        return index
        }
    }
    
    //cancels notification when task is labeled as completed, reschedules notification(if notification was on for that task) when it is set as uncomplete again
    func manageNotifs(task:Task){
        let center = UNUserNotificationCenter.current()
        if !task.completed{ //if the button is unchecked again i need to reschedule the notification
            //create notif
            let content = UNMutableNotificationContent()
            content.title = task.taskName ?? "No title"
            content.subtitle = task.category?.name ?? " "
            content.body = task.moreInfo ?? " "
            content.sound=UNNotificationSound.default()
            let date = task.date! as Date
            let components = Calendar.current.dateComponents([.day,.hour,.minute,.month], from: date)
            print("components: \(components)")
            let trigger = UNCalendarNotificationTrigger(dateMatching: components, repeats: false)
            let request = UNNotificationRequest(identifier: task.uuid!, content: content, trigger: trigger)
            
            center.add(request) { (error) in
                if error != nil{
                    print("an error occured")
                }
            }
            print("notif created")
        
        
        }else{
            center.getPendingNotificationRequests { (requests) in
                for request in requests{
                    print(request.identifier)
                    if request.identifier == task.uuid{
                        center.removePendingNotificationRequests(withIdentifiers: [task.uuid!])
                        print("notif deleted")
                    }
                }
            }
        
        }
    }
    
    
    
}









