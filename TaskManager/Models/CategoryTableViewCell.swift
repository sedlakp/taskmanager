//
//  CategoryTableViewCell.swift
//  TaskManager
//
//  Created by Petr Sedlák on 29/07/2018.
//  Copyright © 2018 Petr Sedlak. All rights reserved.
//

import UIKit

class CategoryTableViewCell: UITableViewCell {

    @IBOutlet var categoryColorImage: UIImageView!
    
    @IBOutlet var categoryNameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
