//
//  Task+CoreDataClass.swift
//  TaskManager
//
//  Created by Petr Sedlák on 28/07/2018.
//  Copyright © 2018 Petr Sedlak. All rights reserved.
//
//

import Foundation
import CoreData


public class Task: NSManagedObject {

    static let dueDateFormatter: DateFormatter = {
        let formatter=DateFormatter()
        formatter.dateStyle = .short
        formatter.timeStyle = .short
        return formatter
    }()
    
}
