//
//  Extensions.swift
//  TaskManager
//
//  Created by Petr Sedlák on 30/07/2018.
//  Copyright © 2018 Petr Sedlak. All rights reserved.
//

import Foundation
import UIKit

extension String{
    var color:UIColor{
        //gets UIColor from string value
        switch self{
        case "black": return UIColor.black
        case "blue": return UIColor.blue
        case "brown": return UIColor.brown
        case "cyan": return UIColor.cyan
        case "green": return UIColor.green
        case "magenta": return UIColor.magenta
        case "orange": return UIColor.orange
        case "purple": return UIColor.purple
        case "red": return UIColor.red
        case "yellow": return UIColor.yellow
        default: return UIColor.black
        }
    }
    
    
    
}
