//
//  SortOption+CoreDataProperties.swift
//  TaskManager
//
//  Created by Petr Sedlák on 01/08/2018.
//  Copyright © 2018 Petr Sedlak. All rights reserved.
//
//

import Foundation
import CoreData


extension SortOption {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<SortOption> {
        return NSFetchRequest<SortOption>(entityName: "SortOption")
    }

    @NSManaged public var value: Int16

}
