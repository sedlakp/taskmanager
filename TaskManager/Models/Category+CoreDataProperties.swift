//
//  Category+CoreDataProperties.swift
//  TaskManager
//
//  Created by Petr Sedlák on 30/07/2018.
//  Copyright © 2018 Petr Sedlak. All rights reserved.
//
//

import Foundation
import CoreData


extension Category {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Category> {
        return NSFetchRequest<Category>(entityName: "Category")
    }

    @NSManaged public var name: String?
    @NSManaged public var color: String?

}
