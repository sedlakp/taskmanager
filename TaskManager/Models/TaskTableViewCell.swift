//
//  TaskTableViewCell.swift
//  TaskManager
//
//  Created by Petr Sedlák on 28/07/2018.
//  Copyright © 2018 Petr Sedlak. All rights reserved.
//

import UIKit

protocol TaskCellDelegate{
    func taskCompleted(sender: TaskTableViewCell)
}

class TaskTableViewCell: UITableViewCell {

    var delegate:TaskCellDelegate?
    
    @IBOutlet var taskNameLabel: UILabel!
    @IBOutlet var dateLabel: UILabel!
    @IBOutlet var categoryLabel: UILabel!
    @IBOutlet var completionButton: UIButton!
    
    @IBAction func completionButtonTapped(_ sender: UIButton) {
        
        delegate?.taskCompleted(sender: self)
        
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
