//
//  DataService.swift
//  TaskManager
//
//  Created by Petr Sedlák on 28/07/2018.
//  Copyright © 2018 Petr Sedlak. All rights reserved.
//

import Foundation
import CoreData

class DataService {
    
    
   
    //
    // MARK: - Core Data stack
    
    static var persistentContainer: NSPersistentContainer = {
      
        let container = NSPersistentContainer(name: "TaskManager")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
            
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    static var context = DataService.persistentContainer.viewContext
    //lazy var context:NSManagedObjectContext{
    //    return persistentContainer.viewContext
   // }
    
    // MARK: - Core Data Saving support
    
    static func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
                print("Saved successfully")
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    
    //function that fetches(gets) data of seleted type and returns array of instances of that type
    static func get<T: NSManagedObject> (type: T.Type) -> [T] {
        let typeName = String(describing: type)
        let getRequest = NSFetchRequest<NSFetchRequestResult>(entityName: typeName)
        
        do {
            let data = try DataService.context.fetch(getRequest) as? [T]
            return data ?? [T]()
        } catch  {
            print(error)
            return [T]()
        }
    }
    
    
    

}











