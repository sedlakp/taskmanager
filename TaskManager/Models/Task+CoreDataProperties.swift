//
//  Task+CoreDataProperties.swift
//  TaskManager
//
//  Created by Petr Sedlák on 01/08/2018.
//  Copyright © 2018 Petr Sedlak. All rights reserved.
//
//

import Foundation
import CoreData


extension Task {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Task> {
        return NSFetchRequest<Task>(entityName: "Task")
    }

    @NSManaged public var categoryName: String?
    @NSManaged public var completed: Bool
    @NSManaged public var date: NSDate?
    @NSManaged public var moreInfo: String?
    @NSManaged public var notification: Bool
    @NSManaged public var taskName: String?
    @NSManaged public var uuid: String?
    @NSManaged public var category: Category?

}
