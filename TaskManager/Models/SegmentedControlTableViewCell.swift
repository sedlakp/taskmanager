//
//  SegmentedControlTableViewCell.swift
//  TaskManager
//
//  Created by Petr Sedlák on 29/07/2018.
//  Copyright © 2018 Petr Sedlak. All rights reserved.
//

import UIKit

protocol SegmentedCellDelegate{
    func sortingValueChanged(sender: SegmentedControlTableViewCell)
}

class SegmentedControlTableViewCell: UITableViewCell {

    var delegate:SegmentedCellDelegate?
    
    @IBOutlet var sortSegmentedControl: UISegmentedControl!
    
    @IBAction func sortingOptionChanged(_ sender: Any) {
        delegate?.sortingValueChanged(sender: self)
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
